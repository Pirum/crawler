crawler
=======

Narzędzie, które na podstawie pierwszych trzech stron wyników wyszukiwania dla zadanego kierunku z url: http://www.wakacje.pl/wczasy/<kierunek> będzie wyznaczało ofertę z najwyższą przeceną dla danego dnia (mechanizm uruchamiany raz dziennie dla wybranego kierunku). Jeżeli występuje kilka ofert z tą samą najwyższą przeceną, zwraca pierwszy wynik. Jeżeli dla danego kierunku w danym momencie nie ma ofert - nic nie zwraca.

Wyniki odkładają się w bazie danych. Wynikiem jest URL do strony z konkretną ofertą.

Mechanizm można uruchamiać z parametrem GET w URL'u aby określić lokalizację - przykład: http://localhost/crawler.pl/?location=<lokalizacja>