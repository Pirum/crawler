<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DomCrawler\Crawler;
use AppBundle\Entity\Discount;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);        
    }
    
    /**
     * @Route("/crawler.php", name="crawler")
     */
    public function setUrlAction(Request $request)
    {
        // use Crawler to get URLs
        // return array of URLs
        function getUrl($p = 1) {
            $html = file_get_contents('http://www.wakacje.pl/wczasy/'.$_GET["location"].'/?str-' . $p);
    
            $crawler = new Crawler($html);        
            $nodes = $crawler->filter('div#gridWithPagination a.black');

            return $nodes->each(function ($item) {
                $hrefUrl = (string) "http://www.wakacje.pl" . $item->attr('href');

                return $hrefUrl;
            });
        }   
        
        // use Crawlert to get highest discount price (in percent)
        // return array of Discounts Value
        function getDiscount($p = 1) {
            $html = file_get_contents('http://www.wakacje.pl/wczasy/'.$_GET["location"].'/?str-' . $p);
    
            $crawler = new Crawler($html);        
            $nodes = $crawler->filter('div#gridWithPagination a.black');

            return $nodes->each(function ($item) {
                $discountValue = get_string_between($item->text(), '-', '%');
                return $discountValue;
            });
        }  
        
        /**
         * Below function has been copied from
         * http://www.justin-cook.com/wp/2006/03/31/php-parse-a-string-between-two-strings/
         * used for get discount value from string
         */
        function get_string_between($string, $start, $end){
            $string = ' ' . $string;
            $ini = strpos($string, $start);
            if ($ini == 0) return 0;
            $ini += strlen($start);
            $len = strpos($string, $end, $ini) - $ini;
            return (string) substr($string, $ini, $len);
        }
     
        // initiate URLs Array from 3 pages
        $url = array_merge(getUrl(1), getUrl(2), getUrl(3));
        // ititate Discoutns Array from 3 pages
        $discount = array_merge(getDiscount(1), getDiscount(2), getDiscount(3));
        // find maximum value of Discount and return Array key
        $maxs = array_keys($discount, max($discount));
        
        // we want to store data only if some offer have promo offer, otherwise do not store value in db
        if (max($discount) > 0) {
            // create new object for store value in DB
            $entity = new Discount();
            $entity->setUrl($url[$maxs[0]]);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
        }
        
        return $this->render('default/crawlerResult.html.twig', array(
            'przecena' => max($discount),
            'url' => $url[$maxs[0]]
        ));
        
    }
}
